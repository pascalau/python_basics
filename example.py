#!/usr/bin/env python

# The shebang ^ tells the operating system what kind of script is executed. Include these in every *.py file.

import numpy as np
import matplotlib.pyplot as plt

# Importing libraries in python is done with the import command 
# With the as * you can shorten the syntax to call functions from this library

# A function definition in python. Notice the indented sections.
# In python indents replace the usual curly brackets from C++!
def a_function(x):
    return np.sin(x)


# A class definition
class a_class:
    # The initilising function
    def __init__(self,x,y,z):
        self.x = x
        self.y = y
        self.z = z

    # A member function
    def member_function(self,x):
        return np.cos(x)

# This statement checks if the currently executed file is executed or not.
# This allows the function to be imported into another file to inherit functions.
# Otherwise when calling the import function the whole file will be executed.
# Additionaly it is a good way to signal what is the main function of the program.
if __name__ == '__main__':

    # A simple for loop in python equivalent to for(int i=0; i<10,i++)
    for i in range(0,10):
        print(i)

    # A if / else statement
    if 1<2:
        print("2 is still bigger than 1")
    elif 2<3:
        print("strange that this statement got printed")
    else:
        print("something went wrong")

    # A list containing some values
    List = [0,1,2,4,8,16,32,64,128,254]

    # Creating an numpy array from a list
    y_1 = np.array(List)

    # This shows the slicing of a list. This way you can easily extract values.
    # This can be used for efficient operations on all kind of lists or arrays.
    # The syntax is [start:stop:step]
    print(List[0:-1:2])
    
    # Numpy default = doesn't create a new array. It makes an alias of y_1
    y_2 = y_1.copy()
    
    # Efficient manipulation of values in y_2 if not all values have to be changed
    y_2[5:10] = y_2[5:10]*3
    print(y_2)

    # Creating an numpy array from multiple Lists for a matrix
    A = np.array([List,List])

    # This creates a vector with uniformly placed values from 0 to 10 with stepsize 1
    x = np.linspace(0,1,10)

    # This command plots the values x,y with various styling options
    plt.plot(x,y_1)
    plt.plot(x,y_1,'bo',label="bluedots")
    plt.plot(x,y_2,'rx',label="red x's")

    # This creates a legend to the plot
    plt.legend()

    # This shows the plot
    plt.show()
